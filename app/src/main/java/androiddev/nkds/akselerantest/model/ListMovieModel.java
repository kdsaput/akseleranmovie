package androiddev.nkds.akselerantest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListMovieModel {

    @SerializedName("status_message")
    private String statusMessage = "";

    @SerializedName("status_code")
    private int statusCode;

    @SerializedName("page")
    private int page;

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("results")
    private List<Result> resultList;

    public ListMovieModel() {
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public List<Result> getResultList() {
        return resultList;
    }

    public void setResultList(List<Result> resultList) {
        this.resultList = resultList;
    }


    public class Result{

        @SerializedName("poster_path")
        private String posterPath;

        @SerializedName("backdrop_path")
        private String backdrop_path;

        @SerializedName("id")
        private int id;

        @SerializedName("title")
        private String title;

        public Result() {
        }

        public String getBackdrop_path() {
            return backdrop_path;
        }

        public void setBackdrop_path(String backdrop_path) {
            this.backdrop_path = backdrop_path;
        }

        public String getPosterPath() {
            return posterPath;
        }

        public void setPosterPath(String posterPath) {
            this.posterPath = posterPath;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
