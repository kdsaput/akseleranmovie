package androiddev.nkds.akselerantest.modules.movie_detail;

import android.widget.TextView;

import java.util.List;

import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.DetailMovieModel;
import androiddev.nkds.akselerantest.model.VideoModel;

public interface MovieDetailContract {

    interface PresenterContract{

        void getDataMovieDetail();

        void checkOverViewText(TextView textView);

        void getCast();

        void getTrailers();

        void initData(int id);

    }

    interface ViewContract{

        void showLoading();

        void hideLoading();

        void showDataMovieDetail(DetailMovieModel movieModel);

        void expandReadMore();

        void hideReadMore();

        void showErrorDetailMovie(String message);

        void setCastList(List<CastModel.Cast> list);

        void showRvCast();

        void showErrorCast(String message);

        void showRvTrailers();

        void showErrorTrailer(String msg);

        void setTrailerList(List<VideoModel.Result> list);
    }
}
