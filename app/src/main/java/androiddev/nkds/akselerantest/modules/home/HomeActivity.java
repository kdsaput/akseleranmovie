package androiddev.nkds.akselerantest.modules.home;

import androiddev.nkds.akselerantest.adapter.ImageSliderAdapter;
import androiddev.nkds.akselerantest.modules.base.BaseActivity;
import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.adapter.MovieAdapter;
import androiddev.nkds.akselerantest.data.DataManager;
import androiddev.nkds.akselerantest.databinding.ActivityMainBinding;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.modules.base.list_movie.ListMovieApiContract;
import androiddev.nkds.akselerantest.modules.movie_detail.MovieDetailActivity;
import androiddev.nkds.akselerantest.modules.see_all.SeeAllActivity;
import androiddev.nkds.akselerantest.utils.constants.K;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends BaseActivity<ActivityMainBinding> implements HomeContract.ViewContract,
        ListMovieApiContract.ViewContract {

    private HomePresenter presenter;
    private MovieAdapter nowPlayingAdapter, topRatedAdapter, selectedAdapter;
    private int currentPage = 0;
    private static int NUM_PAGES;
    private Timer swipeTimer;
    private boolean isFirstTime = true;

    @Override
    public int contentView() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {

        initNowPlayingAdapter();
        initTopRatedAdapter();
        initSelectedAdapter();
    }

    @Override
    public void initPresenter() {

        presenter = new HomePresenter(this, this);
        if (isFirstTime) {
            isFirstTime = false;
            presenter.initData();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("id",DataManager.can().getIDMovieSelected()+"");
        Log.e("idTemp",DataManager.can().getIDMovieSelectedTemp()+"");
        if (DataManager.can().getIDMovieSelected() != 0 &&
                DataManager.can().getIDMovieSelected() != DataManager.can().getIDMovieSelectedTemp()) {
            DataManager.can().setIDMovieSelectedTemp(DataManager.can().getIDMovieSelected());

            presenter.retryHitApi(K.KEY_SELECTED);
        }

        runTimer();
    }

    @Override
    public void assignListener() {

        // ========================= now playing ===================================================
        nowPlayingAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                goToDetailMovie(HomeActivity.this, nowPlayingAdapter.getListMovieModel().get(pos).getId(),
                        nowPlayingAdapter.getListMovieModel().get(pos).getPosterPath());
            }
        });

        mBinding.tvSeeAllNowPlaying.setOnClickListener(v -> {

            goToSeeAllMovie(K.KEY_NOW_PLAYING, getString(R.string.now_playing));
        });

        mBinding.layoutErrorNowPlay.setOnClickListener(v -> {

            presenter.retryHitApi(K.KEY_NOW_PLAYING);
        });

        //==========================================================================================

        // ========================= top rated ===================================================
        topRatedAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                goToDetailMovie(HomeActivity.this, topRatedAdapter.getListMovieModel().get(pos).getId(),
                        topRatedAdapter.getListMovieModel().get(pos).getPosterPath());
            }
        });

        mBinding.tvSeeAllRated.setOnClickListener(v -> {

            goToSeeAllMovie(K.KEY_TOP_RATED, getString(R.string.top_rated));
        });

        mBinding.layoutErrorRated.setOnClickListener(v -> {

            presenter.retryHitApi(K.KEY_TOP_RATED);
        });

        //==========================================================================================

        // ========================= selected ===================================================
        selectedAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                goToDetailMovie(HomeActivity.this, selectedAdapter.getListMovieModel().get(pos).getId(),
                        selectedAdapter.getListMovieModel().get(pos).getPosterPath());
            }
        });

        mBinding.tvSeeAllSelected.setOnClickListener(v -> {

            goToSeeAllMovie(K.KEY_SELECTED, getString(R.string.selected_for_you));
        });

        mBinding.layoutErrorSelected.setOnClickListener(v -> {

            presenter.retryHitApi(K.KEY_SELECTED);
        });

        //==========================================================================================
    }

    @Override
    public void showLoading() {
        mBinding.lyLoading.lyLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mBinding.lyLoading.lyLoading.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMovie(String message, String tipe) {

        if (tipe.equalsIgnoreCase(K.KEY_NOW_PLAYING)) {
            mBinding.tvErrorNowPlay.setText(message);
            mBinding.layoutErrorNowPlay.setVisibility(View.VISIBLE);
            mBinding.rvMovieNowPlaying.setVisibility(View.GONE);
        } else if (tipe.equalsIgnoreCase(K.KEY_TOP_RATED)) {
            mBinding.tvErrorRated.setText(message);
            mBinding.layoutErrorRated.setVisibility(View.VISIBLE);
            mBinding.rvMovieRated.setVisibility(View.GONE);
        } else {
            mBinding.tvErrorSelected.setText(message);
            mBinding.layoutErrorSelected.setVisibility(View.VISIBLE);
            mBinding.rvMovieSelected.setVisibility(View.GONE);
        }
        presenter.checkIsInitData(tipe);
    }

    @Override
    public void setMovieList(List<ListMovieModel.Result> list, String tipe, boolean isAdded) {

        if (tipe.equalsIgnoreCase(K.KEY_NOW_PLAYING)) {
            nowPlayingAdapter.setListMovieModel(list);
            initImageSlider(list);

        } else if (tipe.equalsIgnoreCase(K.KEY_TOP_RATED)) {
            topRatedAdapter.setListMovieModel(list);
        } else {
            selectedAdapter.setListMovieModel(list);
        }
        presenter.checkIsInitData(tipe);
    }

    @Override
    public void showRvMovie(String tipe) {

        if (tipe.equalsIgnoreCase(K.KEY_NOW_PLAYING)) {
            mBinding.layoutErrorNowPlay.setVisibility(View.GONE);
            mBinding.rvMovieNowPlaying.setVisibility(View.VISIBLE);
        } else if (tipe.equalsIgnoreCase(K.KEY_TOP_RATED)) {
            mBinding.layoutErrorRated.setVisibility(View.GONE);
            mBinding.rvMovieRated.setVisibility(View.VISIBLE);
        } else {
            mBinding.layoutErrorSelected.setVisibility(View.GONE);
            mBinding.rvMovieSelected.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorToast(String message) {

    }

    @Override
    public void loadingIndicatorData(boolean isActive) {

    }

    @Override
    public void showLayoutListSelected() {

        mBinding.layoutSelected.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLayoutListSelected() {
        mBinding.layoutSelected.setVisibility(View.GONE);
    }

    private void initNowPlayingAdapter() {

        nowPlayingAdapter = new MovieAdapter(this);
        nowPlayingAdapter.setShowProgressIndicator(false);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvMovieNowPlaying.setLayoutManager(linearLayout);
        mBinding.rvMovieNowPlaying.setHasFixedSize(true);
        mBinding.rvMovieNowPlaying.setAdapter(nowPlayingAdapter);
    }

    private void initTopRatedAdapter() {

        topRatedAdapter = new MovieAdapter(this);
        topRatedAdapter.setShowProgressIndicator(false);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvMovieRated.setLayoutManager(linearLayout);
        mBinding.rvMovieRated.setHasFixedSize(true);
        mBinding.rvMovieRated.setAdapter(topRatedAdapter);
    }

    private void initSelectedAdapter() {

        selectedAdapter = new MovieAdapter(this);
        selectedAdapter.setShowProgressIndicator(false);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvMovieSelected.setLayoutManager(linearLayout);
        mBinding.rvMovieSelected.setHasFixedSize(true);
        mBinding.rvMovieSelected.setAdapter(selectedAdapter);
    }

    private void goToSeeAllMovie(String tipe, String title) {

        Intent intent = new Intent(HomeActivity.this, SeeAllActivity.class);
        intent.putExtra(K.KEY_TIPE, tipe);
        intent.putExtra(K.KEY_TITLE, title);
        startActivity(intent);
    }

    private void initImageSlider(List<ListMovieModel.Result> resultList) {

        swipeTimer = new Timer();
        ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(this, resultList);
        mBinding.layoutImage.setAdapter(imageSliderAdapter);
        mBinding.layoutImage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        NUM_PAGES = resultList.size();

        runTimer();

        imageSliderAdapter.setOnItemClickListener(new ImageSliderAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                goToDetailMovie(HomeActivity.this,
                        resultList.get(pos).getId(), resultList.get(pos).getPosterPath());
            }
        });
    }

    private void stopTimer() {

        if (swipeTimer != null) {
            swipeTimer.cancel();
            swipeTimer.purge();
            swipeTimer = new Timer();
        }
    }

    private void runTimer() {

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mBinding.layoutImage.setCurrentItem(currentPage++, true);
            }
        };

        if (swipeTimer != null) {
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000, 3000);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
    }
}
