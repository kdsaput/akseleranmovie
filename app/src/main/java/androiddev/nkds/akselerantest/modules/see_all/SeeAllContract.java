package androiddev.nkds.akselerantest.modules.see_all;

public interface SeeAllContract {

    interface ViewContract{

        void showLoading();

        void hideLoading();
    }

    interface PresenterContract{

        void getListMovie(String tipe);

        void loadMore(String tipe);

        void checkIsLoadMore();
    }
}
