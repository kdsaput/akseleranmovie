package androiddev.nkds.akselerantest.modules.base.list_movie;

import androiddev.nkds.akselerantest.data.DataManager;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.services.NetworkingFunction;
import androiddev.nkds.akselerantest.services.NetworkingFunctionListener;
import androiddev.nkds.akselerantest.utils.constants.K;
import androiddev.nkds.akselerantest.utils.constants.S;

public class ListMovieApiPresenter implements ListMovieApiContract.PresenterContract,
        NetworkingFunctionListener.GetListMovieListener{

    private ListMovieApiContract.ViewContract viewContract;
    private NetworkingFunction networkingFunction;
    private boolean isAddedToList = false, isNeedLoadMore, isError = false;
    private int page = 1, totalPage = 1;

    public ListMovieApiPresenter(ListMovieApiContract.ViewContract viewContract, boolean isNeedLoadMore){
        this.viewContract = viewContract;
        this.isNeedLoadMore = isNeedLoadMore;
        networkingFunction = new NetworkingFunction();
        networkingFunction.setGetListMovieListener(this);
    }

    @Override
    public void getListMovie(String tipe) {
        networkingFunction.getMovieList(1,tipe);
    }

    @Override
    public void loadMore(String tipe) {

        if(page != totalPage) {

            if (!isError) {
                page += 1;
            }

            isAddedToList = true;
            networkingFunction.getMovieList(page,tipe);
        }
    }

    @Override
    public void onSuccessGetMovie(ListMovieModel model,String tipe) {

        isError = false;
        if (model.getResultList().size() > 0) {
            viewContract.showRvMovie(tipe);
            viewContract.setMovieList(model.getResultList(), tipe,isAddedToList);
            if(isNeedLoadMore) {
                totalPage = model.getTotal_pages();
                if (page != totalPage) {
                    viewContract.loadingIndicatorData(true);
                } else {
                    viewContract.loadingIndicatorData(false);
                }
            }
        } else {
            viewContract.showErrorMovie(S.ERROR_MOVIE_NOT_FOUND, tipe);
        }
    }

    @Override
    public void onFailedGetMovie(String message,String tipe) {
        isError = true;
        checkErrorState(message,tipe);
    }

    private void checkErrorState(String message,String tipe){

        if(isAddedToList){
            viewContract.showErrorToast(message);
        }
        else {
            viewContract.showErrorMovie(message, tipe);
        }
    }
}
