package androiddev.nkds.akselerantest.modules.base.list_movie;

import java.util.List;

import androiddev.nkds.akselerantest.model.ListMovieModel;

public interface ListMovieApiContract {

    interface ViewContract{

        void showErrorMovie(String message, String tipe);

        void setMovieList(List<ListMovieModel.Result> list, String tipe, boolean isAddToList);

        void showRvMovie(String tipe);

        void showErrorToast(String message);

        void loadingIndicatorData(boolean isActive);
    }

    interface PresenterContract{

        void getListMovie(String tipe);

        void loadMore(String tipe);
    }
}
