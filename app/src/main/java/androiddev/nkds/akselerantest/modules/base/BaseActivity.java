package androiddev.nkds.akselerantest.modules.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.data.DataManager;
import androiddev.nkds.akselerantest.modules.home.HomeActivity;
import androiddev.nkds.akselerantest.modules.movie_detail.MovieDetailActivity;
import androiddev.nkds.akselerantest.utils.constants.K;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

@SuppressLint("Registered")
public abstract class BaseActivity<B extends ViewDataBinding> extends AppCompatActivity {

    protected B mBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
        initView();
        initPresenter();
        assignListener();
    }

    protected void loadFragment(Fragment fragment, int fragmentID) {
        // load fragment
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(fragmentID, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /*
     *   set data binding
     */
    private void bindView() {
        mBinding = DataBindingUtil.setContentView(this, contentView());
    }


    /*
     *   set layout xml
     */
    @LayoutRes
    public abstract int contentView();


    /*
     *   inisialisasi semua variable atau class
     */
    public abstract void initView();


    /*
     *   inisialisasi presenter
     */
    public abstract void initPresenter();


    /*
     *   semua function untuk action seperti onclick dll
     */
    public abstract void assignListener();

    protected void showFragmentDialog(Fragment fragment, Bundle bundle) {
        Log.e("tes","tes");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_dialog, fragment);
        fragment.setArguments(bundle);
        transaction.commit();

    }

    protected void goToDetailMovie(Context context,int id, String path){

        Intent intent = new Intent(context, MovieDetailActivity.class);
        intent.putExtra(K.KEY_ID_MOVIE, id);
        intent.putExtra(K.KEY_PATH_POSTER, path);
        DataManager.can().setIDMovieSelected(id);
        startActivity(intent);
    }
}
