package androiddev.nkds.akselerantest.modules.home;

import java.util.List;

import androiddev.nkds.akselerantest.model.ListMovieModel;

public interface HomeContract {

    interface ViewContract{

        void showLoading();

        void hideLoading();

        void showLayoutListSelected();

        void hideLayoutListSelected();

    }

    interface PresenterContract{

        void initData();

        void retryHitApi(String tipe);

        void checkIsInitData(String tipe);
    }
}
