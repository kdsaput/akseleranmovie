package androiddev.nkds.akselerantest.modules.see_all;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.adapter.MovieAdapter;
import androiddev.nkds.akselerantest.databinding.ActivityMainBinding;
import androiddev.nkds.akselerantest.databinding.ActivitySeeAllBinding;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.modules.base.BaseActivity;
import androiddev.nkds.akselerantest.modules.base.list_movie.ListMovieApiContract;
import androiddev.nkds.akselerantest.utils.constants.K;
import androiddev.nkds.akselerantest.utils.constants.S;
import androiddev.nkds.akselerantest.utils.helper.LoadMoreScrollView;
import androiddev.nkds.akselerantest.utils.helper.OnLoadMore;
import androidx.recyclerview.widget.GridLayoutManager;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;

public class SeeAllActivity extends BaseActivity<ActivitySeeAllBinding>
        implements SeeAllContract.ViewContract,ListMovieApiContract.ViewContract {

    private MovieAdapter movieAdapter;
    private GridLayoutManager gridLayoutManager;
    private SeeAllPresenter presenter;
    private String tipe = K.KEY_NOW_PLAYING, title = "";

    @Override
    public int contentView() {
        return R.layout.activity_see_all;
    }

    @Override
    public void initView() {

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            tipe = (String) bd.get(K.KEY_TIPE);
            title = (String) bd.get(K.KEY_TITLE);
            mBinding.tvTitle.setText(title);
        }
        initAdapter();
    }

    @Override
    public void initPresenter() {

        presenter = new SeeAllPresenter(this,this);
        presenter.getListMovie(tipe);
    }

    @Override
    public void assignListener() {

        mBinding.svScroll.setOnScrollChangeListener(new LoadMoreScrollView(gridLayoutManager, new OnLoadMore() {
            @Override
            public void loadMore() {
                movieAdapter.setShowProgressIndicator(true);
                presenter.loadMore(tipe);
            }
        }));

        movieAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                goToDetailMovie(SeeAllActivity.this,movieAdapter.getListMovieModel().get(pos).getId(),
                        movieAdapter.getListMovieModel().get(pos).getPosterPath());
            }
        });

        mBinding.layoutError.setOnClickListener(v->{
            presenter.getListMovie(tipe);
        });
    }

    @Override
    public void showErrorMovie(String message, String tipe) {

        mBinding.tvError.setText(message);
        mBinding.rvMovie.setVisibility(View.GONE);
        mBinding.layoutError.setVisibility(View.VISIBLE);
        presenter.checkIsLoadMore();
    }

    @Override
    public void setMovieList(List<ListMovieModel.Result> list, String tipe, boolean isAddToList) {

        List<ListMovieModel.Result> currentList = movieAdapter.getListMovieModel();
        List<ListMovieModel.Result> newList = new ArrayList<>();
        if (currentList != null && isAddToList) {
            newList.addAll(currentList);
        }

        newList.addAll(list);
        movieAdapter.setListMovieModel(newList);
        presenter.checkIsLoadMore();
    }

    @Override
    public void showRvMovie(String tipe) {

        mBinding.rvMovie.setVisibility(View.VISIBLE);
        mBinding.layoutError.setVisibility(View.GONE);
    }

    @Override
    public void showErrorToast(String message) {

        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadingIndicatorData(boolean isActive) {

        movieAdapter.setShowProgressIndicator(isActive);
        movieAdapter.notifyDataSetChanged();
    }

    private void initAdapter(){

        movieAdapter = new MovieAdapter(this);
        gridLayoutManager = new GridLayoutManager(this,3);
        mBinding.rvMovie.setLayoutManager(gridLayoutManager);
        mBinding.rvMovie.setHasFixedSize(true);
        mBinding.rvMovie.setAdapter(movieAdapter);
    }

    @Override
    public void showLoading() {
        mBinding.lyLoading.lyLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {

        mBinding.lyLoading.lyLoading.setVisibility(View.GONE);
    }
}
