package androiddev.nkds.akselerantest.modules.see_all;

import androiddev.nkds.akselerantest.modules.base.list_movie.ListMovieApiContract;
import androiddev.nkds.akselerantest.modules.base.list_movie.ListMovieApiPresenter;
import androiddev.nkds.akselerantest.utils.constants.S;

public class SeeAllPresenter implements SeeAllContract.PresenterContract {

    private SeeAllContract.ViewContract viewContract;
    private ListMovieApiPresenter listMovieApiPresenter;
    private boolean isLoadMore = false;

    public SeeAllPresenter(SeeAllContract.ViewContract viewContract, ListMovieApiContract.ViewContract apiContract){

        this.viewContract = viewContract;
        listMovieApiPresenter = new ListMovieApiPresenter(apiContract,true);
    }

    @Override
    public void getListMovie(String tipe) {

        viewContract.showLoading();
        listMovieApiPresenter.getListMovie(tipe);
    }

    @Override
    public void loadMore(String tipe) {

        isLoadMore = true;
        listMovieApiPresenter.loadMore(tipe);
    }

    @Override
    public void checkIsLoadMore() {
        if(!isLoadMore){
            viewContract.hideLoading();
        }
    }
}
