package androiddev.nkds.akselerantest.modules.movie_detail;

import android.widget.TextView;

import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.DetailMovieModel;
import androiddev.nkds.akselerantest.model.VideoModel;
import androiddev.nkds.akselerantest.services.NetworkingFunction;
import androiddev.nkds.akselerantest.services.NetworkingFunctionListener;
import androiddev.nkds.akselerantest.utils.constants.S;

public class MovieDetailPresenter implements MovieDetailContract.PresenterContract,
        NetworkingFunctionListener.GetMovieDetailListener,
        NetworkingFunctionListener.GetMovieCastListener,
        NetworkingFunctionListener.GetMovieTrailerListener {

    private MovieDetailContract.ViewContract viewContract;
    private NetworkingFunction networkingFunction;
    private boolean isInit = false, isLoadMore = false;
    private int id = 0;
    private String genres ="", director = "", umur = "";

    public MovieDetailPresenter(MovieDetailContract.ViewContract viewContract){
        this.viewContract = viewContract;
        networkingFunction = new NetworkingFunction();
        networkingFunction.setGetMovieDetailListener(this);
        networkingFunction.setGetMovieCastListener(this);
        networkingFunction.setGetMovieTrailerListener(this);

    }

    @Override
    public void initData(int id) {

        viewContract.showLoading();
        isInit = true;
        this.id = id;
        hitApiMovieDetail();
    }

    @Override
    public void getDataMovieDetail() {

        viewContract.showLoading();
        isInit = false;
        hitApiMovieDetail();

    }

    @Override
    public void checkOverViewText(TextView textView) {

        if (textView.getText().toString().equalsIgnoreCase(S.SHOW_OVERVIEW))
        {
            viewContract.expandReadMore();
        }
        else
        {
            viewContract.hideReadMore();
        }
    }

    @Override
    public void getCast() {

        viewContract.showLoading();
        isInit = false;
        hitApiCast();
    }


    @Override
    public void getTrailers() {

        viewContract.showLoading();
        isInit = false;
        hitApiTrailer();
    }

    @Override
    public void onSuccessGetDetailMovie(DetailMovieModel model) {

        umur = checkAdult(model.isAdult());
        genres = setStringComma(true,model);
        director =  setStringComma(false,model);
        viewContract.showDataMovieDetail(model);

        if(!isInit){
            viewContract.hideLoading();
        }
        else {
            hitApiCast();
        }
    }

    @Override
    public void onFailedGetDetailMovie(String message) {

        viewContract.showErrorDetailMovie(message);
        if(!isInit){
            viewContract.hideLoading();
        }
        else {
            hitApiCast();
        }
    }

    @Override
    public void onSuccessGetMovieTrailer(VideoModel model) {

        if (model.getResultList().size() > 0) {
            viewContract.showRvTrailers();
            viewContract.setTrailerList(model.getResultList());
        } else {
            viewContract.showErrorTrailer(S.ERROR_TRAILER_NOT_FOUND);
        }
        viewContract.hideLoading();
    }

    @Override
    public void onFailedGetMovieTrailer(String message) {

        viewContract.showErrorTrailer(message);
        viewContract.hideLoading();
    }

    @Override
    public void onSuccessGetMovieCast(CastModel model) {

        if (model.getCastList().size() > 0) {
            viewContract.showRvCast();
            viewContract.setCastList(model.getCastList());
        } else {
            viewContract.showErrorCast(S.ERROR_CAST_NOT_FOUND);
        }

        if(!isInit){
            viewContract.hideLoading();
        }
        else {
            hitApiTrailer();
        }
    }

    @Override
    public void onFailedGetMovieCast(String message) {

        viewContract.showErrorCast(message);
        if(!isInit){
            viewContract.hideLoading();
        }
        else {
            hitApiTrailer();
        }
    }

    private void hitApiMovieDetail(){
        networkingFunction.getDetailMovie(id);
    }

    private void hitApiCast(){
        networkingFunction.getMovieCast(id);
    }

    private void hitApiTrailer(){
        networkingFunction.getMovieTrailer(id);
    }

    private String setStringComma(boolean isGenre, DetailMovieModel detailMovieModel){

        int size = 0;
        String hasil = "";
        if(isGenre){
            if(detailMovieModel.getGenres()!=null && detailMovieModel.getGenres().size()>0) {

                size = detailMovieModel.getGenres().size();
                if(size!=0){
                    for(int i = 0; i<size; i++){
                        if(size - (i+1) !=0){
                            hasil = hasil + detailMovieModel.getGenres().get(i).getName() +", ";
                        }
                        else {
                            hasil = hasil+ detailMovieModel.getGenres().get(i).getName();
                        }
                    }
                }
            }
        }
        else {
            if(detailMovieModel.getProductionCompanies()!=null && detailMovieModel.getProductionCompanies().size()>0) {
                size = detailMovieModel.getProductionCompanies().size();
                if(size!=0){
                    for(int i = 0; i<size; i++){
                        if(size - (i+1) !=0){
                            hasil = hasil + detailMovieModel.getProductionCompanies().get(i).getName() +", ";
                        }
                        else {
                            hasil = hasil + detailMovieModel.getProductionCompanies().get(i).getName();
                        }
                    }
                }
            }
        }

        return hasil;
    }

    private String checkAdult(boolean isAdult){
        if(isAdult){
            return S.UMUR_18;
        }
        return  S.UMUR_13;
    }

    public String getGenres() {
        return genres;
    }

    public String getDirector() {
        return director;
    }

    public String getUmur() {
        return umur;
    }

}
