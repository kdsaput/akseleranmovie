package androiddev.nkds.akselerantest.modules.movie_detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import java.util.List;

import androiddev.nkds.akselerantest.dialog.DialogDetailCast;
import androiddev.nkds.akselerantest.modules.base.BaseActivity;
import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.adapter.CastAdapter;
import androiddev.nkds.akselerantest.adapter.TrailerAdapter;
import androiddev.nkds.akselerantest.databinding.ActivityMovieDetailBinding;
import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.DetailMovieModel;
import androiddev.nkds.akselerantest.model.VideoModel;
import androiddev.nkds.akselerantest.utils.constants.K;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.recyclerview.widget.LinearLayoutManager;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;
import static androiddev.nkds.akselerantest.utils.helper.StringHelper.changeFromMinutesToHour;

public class MovieDetailActivity extends BaseActivity<ActivityMovieDetailBinding>
        implements MovieDetailContract.ViewContract {

    private int id = 0;
    private String path = "";
    private MovieDetailPresenter presenter;
    private TrailerAdapter trailerAdapter;
    private CastAdapter castAdapter;

    @Override
    public int contentView() {
        return R.layout.activity_movie_detail;
    }

    @Override
    public void initView() {

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            id = (int) bd.get(K.KEY_ID_MOVIE);
            path = (String) bd.get(K.KEY_PATH_POSTER);
            if (path != null && !path.equalsIgnoreCase("")) {
                setImageToImageView(this, mBinding.ivPoster, S.PATH_BASE_URL + path);
            }
        }

        initCastAdapter();
        initTrailerAdapter();
    }

    @Override
    public void initPresenter() {

        presenter = new MovieDetailPresenter(this);
        presenter.initData(id);
    }

    @Override
    public void assignListener() {

        mBinding.tvReadMore.setOnClickListener(v->{
            presenter.checkOverViewText(mBinding.tvReadMore);
        });


        //==================================== trailer =============================================
        trailerAdapter.setOnItemClickListener(new TrailerAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                String id = trailerAdapter.getListTrailer().get(pos).getKey();
                Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"+id));
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+id));

                try {
                    startActivity(appIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    startActivity(webIntent);
                }
            }
        });
        //===================================== trailer ============================================

        //===================================== cast ===============================================
        castAdapter.setOnItemClickListener(new CastAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(int pos) {

                Bundle bundle = new Bundle();
                bundle.putString(K.KEY_PATH_POSTER, castAdapter.getListTrailer().get(pos).getProfile_path());
                bundle.putString(K.KEY_NAME, castAdapter.getListTrailer().get(pos).getName());
                bundle.putString(K.KEY_GENDER, castAdapter.getListTrailer().get(pos).getGender());
                bundle.putString(K.KEY_CHARACTER, castAdapter.getListTrailer().get(pos).getCharacter());
                showFragmentDialog(new DialogDetailCast(), bundle);
            }
        });
    }

    @Override
    public void showLoading() {

        mBinding.lyLoading.lyLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {

        mBinding.lyLoading.lyLoading.setVisibility(View.GONE);
    }

    @Override
    public void showDataMovieDetail(DetailMovieModel movieModel) {

        mBinding.layoutHeaderRight.setVisibility(View.VISIBLE);
        mBinding.layoutErrorHeader.setVisibility(View.GONE);
        setImageToImageView(this,mBinding.ivBackDrop,S.BACKDROP_BASE_URL+movieModel.getBackdrop_path());
        mBinding.tvTitle.setText(movieModel.getTitle());
        mBinding.tvUmur.setText(presenter.getUmur());
        mBinding.tvDuration.setText(changeFromMinutesToHour(movieModel.getRuntime()));
        mBinding.tvTahunGenre.setText(movieModel.getRelease_date().substring(0,4)+" - "+presenter.getGenres());
        mBinding.tvDirected.setText("Produced by "+presenter.getDirector());
        mBinding.ratingBar.setRating(movieModel.getVote_average() / 2);
        mBinding.tvOverview.setText(movieModel.getOverview());

    }

    @Override
    public void expandReadMore() {

        mBinding.tvReadMore.setText(S.HIDE);
        mBinding.tvOverview.setMaxLines(Integer.MAX_VALUE);
    }

    @Override
    public void hideReadMore() {

        mBinding.tvReadMore.setText(S.SHOW_OVERVIEW);
        mBinding.tvOverview.setMaxLines(2);
    }

    @Override
    public void showErrorDetailMovie(String message) {

        mBinding.tvErrorHeader.setText(message);
        mBinding.layoutErrorHeader.setVisibility(View.VISIBLE);
        mBinding.layoutHeaderRight.setVisibility(View.GONE);
    }

    @Override
    public void setCastList(List<CastModel.Cast> list) {

        castAdapter.setResultList(list);
    }

    @Override
    public void showRvCast() {

        mBinding.layoutErrorCast.setVisibility(View.GONE);
        mBinding.rvCast.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorCast(String message) {

        mBinding.tvErrorCast.setText(message);
        mBinding.layoutErrorCast.setVisibility(View.VISIBLE);
        mBinding.rvCast.setVisibility(View.GONE);
    }

    @Override
    public void showRvTrailers() {

        mBinding.rvTrailer.setVisibility(View.VISIBLE);
        mBinding.layoutErrorTrailer.setVisibility(View.GONE);
    }

    @Override
    public void showErrorTrailer(String msg) {

        mBinding.tvErrorTrailer.setText(msg);
        mBinding.layoutErrorTrailer.setVisibility(View.VISIBLE);
        mBinding.rvTrailer.setVisibility(View.GONE);
    }

    @Override
    public void setTrailerList(List<VideoModel.Result> list) {

        trailerAdapter.setResultList(list);
    }

    private void initCastAdapter(){

        castAdapter = new CastAdapter(this);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvCast.setLayoutManager(linearLayout);
        mBinding.rvCast.setHasFixedSize(true);
        mBinding.rvCast.setAdapter(castAdapter);
    }

    private void initTrailerAdapter(){

        trailerAdapter = new TrailerAdapter(this);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvTrailer.setLayoutManager(linearLayout);
        mBinding.rvTrailer.setHasFixedSize(true);
        mBinding.rvTrailer.setAdapter(trailerAdapter);
    }
}
