package androiddev.nkds.akselerantest.modules.home;

import android.util.Log;

import androiddev.nkds.akselerantest.data.DataManager;
import androiddev.nkds.akselerantest.modules.base.list_movie.ListMovieApiContract;
import androiddev.nkds.akselerantest.modules.base.list_movie.ListMovieApiPresenter;
import androiddev.nkds.akselerantest.utils.constants.K;

public class HomePresenter implements HomeContract.PresenterContract {

    private HomeContract.ViewContract viewContract;
    private int id = 0;
    private boolean isInit = false;
    private ListMovieApiPresenter listMovieApiPresenter;

    public HomePresenter(HomeContract.ViewContract viewContract, ListMovieApiContract.ViewContract apiContract) {
        this.viewContract = viewContract;
        listMovieApiPresenter = new ListMovieApiPresenter(apiContract,false);
    }

    @Override
    public void initData() {
        viewContract.showLoading();
        isInit = true;
        listMovieApiPresenter.getListMovie(K.KEY_NOW_PLAYING);
    }

    @Override
    public void retryHitApi(String tipe) {

        viewContract.showLoading();
        isInit = false;
        listMovieApiPresenter.getListMovie(tipe);

    }

    @Override
    public void checkIsInitData(String tipe) {
        if (tipe.equalsIgnoreCase(K.KEY_NOW_PLAYING)) {
            if (!isInit) {
                viewContract.hideLoading();
            } else {
                listMovieApiPresenter.getListMovie(K.KEY_TOP_RATED);
            }
        }
        else if(tipe.equalsIgnoreCase(K.KEY_TOP_RATED)){
            if (!isInit) {
                viewContract.hideLoading();
            } else {
                if (DataManager.can().getIDMovieSelected() != 0) {
                    viewContract.showLayoutListSelected();
                    listMovieApiPresenter.getListMovie(K.KEY_SELECTED);
                } else {
                    viewContract.hideLayoutListSelected();
                    viewContract.hideLoading();
                }
            }
        }
        else {
            viewContract.hideLoading();
            if(DataManager.can().getIDMovieSelected()!=0){
                viewContract.showLayoutListSelected();
            }
            else {
                viewContract.hideLayoutListSelected();
            }
        }
    }
}
