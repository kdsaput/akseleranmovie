package androiddev.nkds.akselerantest.data;

import androiddev.nkds.akselerantest.data.movie.MovieStorage;

public class DataManager implements DataManagerContract {

    private static DataManager dm;
    public static DataManager can() // or use, or call (?)
    {
        if (dm == null) {
            dm = new DataManager();
        }
        return dm;
    }

    private static MovieStorage movieStorage = new MovieStorage();


    @Override
    public int getIDMovieSelected() {
        return movieStorage.getIDMovieSelected();
    }
    @Override
    public void setIDMovieSelected(int id) {
        movieStorage.setIDMovieSelected(id);
    }

    @Override
    public int getIDMovieSelectedTemp() {
        return movieStorage.getIDMovieSelectedTemp();
    }
    @Override
    public void setIDMovieSelectedTemp(int id) {
        movieStorage.setIDMovieSelectedTemp(id);
    }
}
