package androiddev.nkds.akselerantest.data;

public interface DataManagerContract {

    int getIDMovieSelected();
    void setIDMovieSelected(int id);

    int getIDMovieSelectedTemp();
    void setIDMovieSelectedTemp(int id);

}
