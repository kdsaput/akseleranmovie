package androiddev.nkds.akselerantest.data.movie;

import com.orhanobut.hawk.Hawk;

import androiddev.nkds.akselerantest.utils.constants.K;

public class MovieStorage {

    public void setIDMovieSelected(int id) {
        Hawk.put(K.KEY_ID_MOVIE, id);
    }

    public int getIDMovieSelected() {
        return Hawk.get(K.KEY_ID_MOVIE, 0);
    }

    public void setIDMovieSelectedTemp(int id) {
        Hawk.put(K.KEY_ID_MOVIE_TEMP, id);
    }

    public int getIDMovieSelectedTemp() {
        return Hawk.get(K.KEY_ID_MOVIE_TEMP, 0);
    }

}
