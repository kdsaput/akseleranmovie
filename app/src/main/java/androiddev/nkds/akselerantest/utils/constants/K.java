package androiddev.nkds.akselerantest.utils.constants;

public class K {

    public static final String KEY_ID_MOVIE = "KEY_ID_MOVIE";
    public static final String KEY_ID_MOVIE_TEMP = "KEY_ID_MOVIE_TEMP";
    public static final String KEY_PATH_POSTER = "KEY_PATH_POSTER";
    public static final String KEY_NOW_PLAYING = "KEY_NOW_PLAYING";
    public static final String KEY_TOP_RATED = "KEY_TOP_RATED";
    public static final String KEY_SELECTED = "KEY_SELECTED";
    public static final String KEY_TIPE = "KEY_TIPE";
    public static final String KEY_TITLE = "KEY_TITLE";

    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_GENDER = "KEY_GENDER";
    public static final String KEY_CHARACTER = "KEY_CHARACTER";
}
