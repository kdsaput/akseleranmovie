package androiddev.nkds.akselerantest.utils.helper;

import android.util.Log;

public class StringHelper {

    public static String changeFromMinutesToHour(int minutes){

        String hasil = "";
        Log.e("minut",minutes+"");
        if(minutes>=60){

            hasil = (minutes / 60) + "hr ";
            Log.e("hasil modulus", hasil);
            if(minutes % 60 > 0){

                minutes = minutes % 60;
                Log.e("hasil minut", minutes+"");
                hasil = hasil + " "+ minutes + " mins";
            }

        }
        else {
            hasil = minutes + " mins";
        }


        return hasil;
    }

    public static String changeGenderToCharacters(String gender){

        String hasil = "";
        Log.e("minut",gender);
        int genderInt = 0;
        try {
            genderInt = Integer.parseInt(gender);
            if(genderInt==2){
                hasil = "Male";
            }
            else {
                hasil = "Female";
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return hasil;
    }
}
