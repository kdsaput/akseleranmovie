package androiddev.nkds.akselerantest.utils.helper;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;

public class LoadMoreScrollView implements NestedScrollView.OnScrollChangeListener {

    private LinearLayoutManager layoutManager;

    private OnLoadMore onLoadMore;

    public LoadMoreScrollView(LinearLayoutManager layoutManager, OnLoadMore onLoadMore){

        this.layoutManager = layoutManager;
        this.onLoadMore = onLoadMore;
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if(v.getChildAt(v.getChildCount() - 1) != null) {
            if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    onLoadMore.loadMore();
                }
            }
        }
    }
}
