package androiddev.nkds.akselerantest.utils.constants;

public class S {

    // ============================= url ==================================================
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String PATH_BASE_URL="https://image.tmdb.org/t/p/w185";
    public static final String BACKDROP_BASE_URL="https://image.tmdb.org/t/p/w500";
    public static final String YOUTUBE_IMAGE_URL="https://img.youtube.com/vi/%1$s/hqdefault.jpg";

    public static final String LANGUAGE = "en-US";
    public static final String API_KEY = "fc96fbfe67a4d0380de33554ea33a8a9";
    public static final String ERROR_NO_INTERNET = "No connection internet, \nPlease turn on your mobile data or wifi";
    public static final String ERROR_THROW = "System under maintenance, \nPlease wait and try again later";
    public static final String ERROR_MOVIE_NOT_FOUND = "Movie not found, \nPlease wait and try again later";
    public static final String ERROR_TRAILER_NOT_FOUND = "Trailer not found, \nPlease wait and try again later";
    public static final String ERROR_CAST_NOT_FOUND = "Cast not found, \nPlease wait and try again later";

    public static final String SITE_TRAILER="YouTube";
    public static final String SHOW_OVERVIEW="Read More";
    public static final String HIDE="Hide";

    public static final String UMUR_13="+13";
    public static final String UMUR_18="+18";
}
