package androiddev.nkds.akselerantest;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;

import androiddev.nkds.akselerantest.services.RestClient;

public class MainApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        //NETWORK
        RestClient.start(this);

        //GLOBAL CONTEXT
        MainApplication.context = getApplicationContext();

        //HAWK STORAGE
        Hawk.init(getApplicationContext()).build();
    }

    public static Context getAppContext() {
        return MainApplication.context;
    }
}
