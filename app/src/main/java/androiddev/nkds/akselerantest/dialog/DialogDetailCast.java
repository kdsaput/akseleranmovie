package androiddev.nkds.akselerantest.dialog;

import android.content.Context;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.databinding.DialogDetailCastBinding;
import androiddev.nkds.akselerantest.modules.base.BaseDialogFragment;
import androiddev.nkds.akselerantest.utils.constants.K;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.annotation.NonNull;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;
import static androiddev.nkds.akselerantest.utils.helper.StringHelper.changeGenderToCharacters;

public class DialogDetailCast extends BaseDialogFragment<DialogDetailCastBinding> {

    private String path = "";
    private Context context;
    @Override
    public int inflateView() {
        return R.layout.dialog_detail_cast;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void initView() {

        if (getArguments() != null) {

            path = getArguments().getString(K.KEY_PATH_POSTER);
            setImageToImageView(context,mBinding.ivProfile,S.PATH_BASE_URL+path);

            mBinding.tvName.setText(getArguments().getString(K.KEY_NAME));
            mBinding.tvGender.setText(changeGenderToCharacters(getArguments().getString(K.KEY_GENDER)));
            mBinding.tvCharacter.setText(getArguments().getString(K.KEY_CHARACTER));

        }
        setCancelable(false);
    }

    @Override
    public void assignListener() {

        mBinding.btnClose.setOnClickListener(v->{
            dismiss();
        });
    }
}
