package androiddev.nkds.akselerantest.services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import androiddev.nkds.akselerantest.utils.constants.S;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static RestClient mInstance;

    public static RestClient getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("main application not called");
        }
        return mInstance;
    }

    private RestApi restApi;

    private Retrofit retrofit;

    private RestClient(Context context) {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(S.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(createGson()))
                .client(okHttpClient)
                .build();
    }

    private Gson createGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    public RestApi getApiServices(){
        if(restApi==null){
            restApi =retrofit.create(RestApi.class);
        }
        return restApi;
    }

    public static void start(Context context) {
        mInstance = new RestClient(context);
    }
}
