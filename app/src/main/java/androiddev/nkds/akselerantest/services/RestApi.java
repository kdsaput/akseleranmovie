package androiddev.nkds.akselerantest.services;

import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.DetailMovieModel;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.model.VideoModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestApi {

    @GET("movie/now_playing")
    public Call<ListMovieModel> getNowPlaying(@Query("api_key") String apiKey,
                                              @Query("language") String language,
                                              @Query("page") int page);

    @GET("movie/top_rated")
    public Call<ListMovieModel> getTopRated(@Query("api_key") String apiKey,
                                            @Query("language") String language,
                                            @Query("page") int page);

    @GET("movie/{movie_id}/similar")
    public Call<ListMovieModel> getSimilarMovies(@Path(value = "movie_id", encoded = true) String id,
                                                 @Query("api_key") String apiKey,
                                                 @Query("language") String language,
                                                 @Query("page") int page);

    @GET("movie/{movie_id}")
    public Call<DetailMovieModel> getDetailMovie(@Path(value = "movie_id", encoded = true) String id,
                                                 @Query("api_key") String apiKey,
                                                 @Query("language") String language);

    @GET("movie/{movie_id}/videos")
    public Call<VideoModel> getMovieVideos(@Path(value = "movie_id", encoded = true) String id,
                                           @Query("api_key") String apiKey,
                                           @Query("language") String language);

    @GET("movie/{movie_id}/credits")
    public Call<CastModel> getMovieCredit(@Path(value = "movie_id", encoded = true) String id,
                                          @Query("api_key") String apiKey);
}
