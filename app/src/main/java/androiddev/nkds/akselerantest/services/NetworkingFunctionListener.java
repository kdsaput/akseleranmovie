package androiddev.nkds.akselerantest.services;

import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.DetailMovieModel;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.model.VideoModel;

public class NetworkingFunctionListener {

    public interface GetListMovieListener{
        void onSuccessGetMovie(ListMovieModel model, String tipe);
        void onFailedGetMovie(String message, String tipe);
    }

//    public interface GetListMovieTopRatedListener{
//        void onSuccessGetMovieTop(ListMovieModel model);
//        void onFailedGetMovieTop(String message);
//    }
//
//    public interface GetListMovieSelectedListener{
//        void onSuccessGetMovieSelected(ListMovieModel model);
//        void onFailedGetMovieSelected(String message);
//    }

    public interface GetMovieDetailListener{
        void onSuccessGetDetailMovie(DetailMovieModel model);
        void onFailedGetDetailMovie(String message);
    }

    public interface GetMovieTrailerListener{
        void onSuccessGetMovieTrailer(VideoModel model);
        void onFailedGetMovieTrailer(String message);
    }

    public interface GetMovieCastListener{
        void onSuccessGetMovieCast(CastModel model);
        void onFailedGetMovieCast(String message);
    }
}
