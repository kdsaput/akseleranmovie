package androiddev.nkds.akselerantest.services;

import android.util.Log;

import com.google.gson.Gson;

import androiddev.nkds.akselerantest.data.DataManager;
import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.DetailMovieModel;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.model.VideoModel;
import androiddev.nkds.akselerantest.utils.constants.K;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androiddev.nkds.akselerantest.utils.helper.InternetConnectionHelper.checkConnection;

public class NetworkingFunction {

    private RestApi restApi;

    private NetworkingFunctionListener.GetListMovieListener getListMovieListener;
//    private NetworkingFunctionListener.GetListMovieTopRatedListener getListMovieTopRatedListener;
//    private NetworkingFunctionListener.GetListMovieSelectedListener getListMovieSelectedListener;
    private NetworkingFunctionListener.GetMovieDetailListener getMovieDetailListener;
    private NetworkingFunctionListener.GetMovieTrailerListener getMovieTrailerListener;
    private NetworkingFunctionListener.GetMovieCastListener getMovieCastListener;

    public NetworkingFunction(){

        restApi = RestClient.getInstance().getApiServices();
    }

    public void getMovieList(int page, String tipe){

        if(checkConnection()){

            Call<ListMovieModel> call = null;
            if(tipe.equalsIgnoreCase(K.KEY_NOW_PLAYING)) {
                call = restApi.getNowPlaying(S.API_KEY, S.LANGUAGE, page);
            }
            else if(tipe.equalsIgnoreCase(K.KEY_TOP_RATED)){
                call = restApi.getTopRated(S.API_KEY, S.LANGUAGE, page);
            }
            else {
                call = restApi.getSimilarMovies(DataManager.can().getIDMovieSelected()+"",
                        S.API_KEY, S.LANGUAGE, page);
            }
            call.enqueue(new Callback<ListMovieModel>() {
                @Override
                public void onResponse(Call<ListMovieModel> call, Response<ListMovieModel> response) {

                    if(response.body()!=null) {
                        if (!response.isSuccessful()) {
                            if(response.body().getStatusMessage()!=null) {
                                getListMovieListener.onFailedGetMovie(response.body().getStatusMessage(),tipe);
                            }
                            else {
                                getListMovieListener.onFailedGetMovie(S.ERROR_THROW,tipe);
                            }
                        } else {

                            Log.e("hasil", new Gson().toJson(response.body()));
                            if (response.body().getResultList() != null &&
                                    response.body().getResultList().size()>0) {
                                getListMovieListener.onSuccessGetMovie(response.body(),tipe);

                            } else {
                                getListMovieListener.onFailedGetMovie(S.ERROR_MOVIE_NOT_FOUND,tipe);
                            }
                        }
                    }
                    else {
                        getListMovieListener.onFailedGetMovie(S.ERROR_THROW,tipe);
                    }
                }

                @Override
                public void onFailure(Call<ListMovieModel> call, Throwable t) {
                    if(t.getMessage()!=null) {
                        getListMovieListener.onFailedGetMovie(t.getMessage(),tipe);
                    }
                    else {
                        getListMovieListener.onFailedGetMovie(S.ERROR_THROW,tipe);
                    }
                }
            });
        }
        else {
            getListMovieListener.onFailedGetMovie(S.ERROR_NO_INTERNET,tipe);
        }
    }

//    public void getTopRated(int page){
//
//        if(checkConnection()){
//
//            Call<ListMovieModel> call = restApi.getTopRated(S.API_KEY,S.LANGUAGE,page);
//            call.enqueue(new Callback<ListMovieModel>() {
//                @Override
//                public void onResponse(@NonNull Call<ListMovieModel> call, Response<ListMovieModel> response) {
//
//                    if(response.body()!=null) {
//                        if (!response.isSuccessful()) {
//                            Log.e("hasil", new Gson().toJson(response.body()));
//                            if(response.body().getStatusMessage()!=null) {
//                                getListMovieTopRatedListener.onFailedGetMovieTop(response.body().getStatusMessage());
//                            }
//                            else {
//                                getListMovieTopRatedListener.onFailedGetMovieTop(S.ERROR_THROW);
//                            }
//                        } else {
//
//                            Log.e("hasil top", new Gson().toJson(response.body()));
//                            if (response.body().getResultList() != null &&
//                                    response.body().getResultList().size()>0) {
//                                getListMovieTopRatedListener.onSuccessGetMovieTop(response.body());
//
//                            } else {
//                                getListMovieTopRatedListener.onFailedGetMovieTop(S.ERROR_MOVIE_NOT_FOUND);
//                            }
//                        }
//                    }
//                    else {
//                        getListMovieTopRatedListener.onFailedGetMovieTop(S.ERROR_THROW);
//                    }
//                }
//
//                @Override
//                public void onFailure(@NonNull Call<ListMovieModel> call, Throwable t) {
//                    if(t.getMessage()!=null) {
//                        getListMovieTopRatedListener.onFailedGetMovieTop(t.getMessage());
//                    }
//                    else {
//                        getListMovieTopRatedListener.onFailedGetMovieTop(S.ERROR_THROW);
//                    }
//                }
//            });
//        }
//        else {
//            getListMovieTopRatedListener.onFailedGetMovieTop(S.ERROR_NO_INTERNET);
//        }
//    }
//
//    public void getSelected(int id,int page){
//
//        if(checkConnection()){
//
//            Call<ListMovieModel> call = restApi.getSimilarMovies(id+"",S.API_KEY,S.LANGUAGE,page);
//            call.enqueue(new Callback<ListMovieModel>() {
//                @Override
//                public void onResponse(@NonNull Call<ListMovieModel> call, Response<ListMovieModel> response) {
//
//                    if(response.body()!=null) {
//                        if (!response.isSuccessful()) {
//                            Log.e("hasil", new Gson().toJson(response.body()));
//                            if(response.body().getStatusMessage()!=null) {
//                                getListMovieSelectedListener.onFailedGetMovieSelected(response.body().getStatusMessage());
//                            }
//                            else {
//                                getListMovieSelectedListener.onFailedGetMovieSelected(S.ERROR_THROW);
//                            }
//                        } else {
//
//                            Log.e("hasil top", new Gson().toJson(response.body()));
//                            if (response.body().getResultList() != null &&
//                                    response.body().getResultList().size()>0) {
//                                getListMovieSelectedListener.onSuccessGetMovieSelected(response.body());
//
//                            } else {
//                                getListMovieSelectedListener.onFailedGetMovieSelected(S.ERROR_MOVIE_NOT_FOUND);
//                            }
//                        }
//                    }
//                    else {
//                        getListMovieSelectedListener.onFailedGetMovieSelected(S.ERROR_THROW);
//                    }
//                }
//
//                @Override
//                public void onFailure(@NonNull Call<ListMovieModel> call, Throwable t) {
//                    if(t.getMessage()!=null) {
//                        getListMovieSelectedListener.onFailedGetMovieSelected(t.getMessage());
//                    }
//                    else {
//                        getListMovieSelectedListener.onFailedGetMovieSelected(S.ERROR_THROW);
//                    }
//                }
//            });
//        }
//        else {
//            getListMovieSelectedListener.onFailedGetMovieSelected(S.ERROR_NO_INTERNET);
//        }
//    }

    public void getDetailMovie(int id){

        if(checkConnection()){

            Call<DetailMovieModel> call = restApi.getDetailMovie(id+"",S.API_KEY,S.LANGUAGE);
            call.enqueue(new Callback<DetailMovieModel>() {
                @Override
                public void onResponse(@NonNull Call<DetailMovieModel> call, Response<DetailMovieModel> response) {

                    if(response.body()!=null) {
                        if (!response.isSuccessful()) {

                            if(response.body().getStatusMessage()!=null) {
                                getMovieDetailListener.onFailedGetDetailMovie(response.body().getStatusMessage());
                            }
                            else {
                                getMovieDetailListener.onFailedGetDetailMovie(S.ERROR_THROW);
                            }
                        } else {

                            Log.e("hasil detail", new Gson().toJson(response.body()));
                            getMovieDetailListener.onSuccessGetDetailMovie(response.body());
                        }
                    }
                    else {
                        getMovieDetailListener.onFailedGetDetailMovie(S.ERROR_THROW);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DetailMovieModel> call, Throwable t) {
                    if(t.getMessage()!=null) {
                        getMovieDetailListener.onFailedGetDetailMovie(t.getMessage());
                    }
                    else {
                        getMovieDetailListener.onFailedGetDetailMovie(S.ERROR_THROW);
                    }
                }
            });
        }
        else {
            getMovieDetailListener.onFailedGetDetailMovie(S.ERROR_NO_INTERNET);
        }
    }

    public void getMovieCast(int id){

        if(checkConnection()){

            Call<CastModel> call = restApi.getMovieCredit(id+"",S.API_KEY);
            call.enqueue(new Callback<CastModel>() {
                @Override
                public void onResponse(@NonNull Call<CastModel> call, Response<CastModel> response) {

                    if(response.body()!=null) {
                        if (!response.isSuccessful()) {

                            if(response.body().getStatusMessage()!=null) {
                                getMovieCastListener.onFailedGetMovieCast(response.body().getStatusMessage());
                            }
                            else {
                                getMovieCastListener.onFailedGetMovieCast(S.ERROR_THROW);
                            }
                        } else {

                            if (response.body().getCastList() != null &&
                                    response.body().getCastList().size()>0) {
                                Log.e("hasil review", new Gson().toJson(response.body()));
                                getMovieCastListener.onSuccessGetMovieCast(response.body());

                            } else {
                                Log.e("error","masuk sini");
                                getMovieCastListener.onFailedGetMovieCast(S.ERROR_CAST_NOT_FOUND);
                            }
                        }
                    }
                    else {
                        getMovieCastListener.onFailedGetMovieCast(S.ERROR_THROW);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CastModel> call, Throwable t) {
                    if(t.getMessage()!=null) {
                        getMovieCastListener.onFailedGetMovieCast(t.getMessage());
                    }
                    else {
                        getMovieCastListener.onFailedGetMovieCast(S.ERROR_THROW);
                    }
                }
            });
        }
        else {
            getMovieCastListener.onFailedGetMovieCast(S.ERROR_NO_INTERNET);
        }
    }

    public void getMovieTrailer(int id){

        if(checkConnection()){

            Call<VideoModel> call = restApi.getMovieVideos(id+"",S.API_KEY,S.LANGUAGE);
            call.enqueue(new Callback<VideoModel>() {
                @Override
                public void onResponse(@NonNull Call<VideoModel> call, Response<VideoModel> response) {

                    if(response.body()!=null) {
                        if (!response.isSuccessful()) {

                            if(response.body().getStatusMessage()!=null) {
                                getMovieTrailerListener.onFailedGetMovieTrailer(response.body().getStatusMessage());
                            }
                            else {
                                getMovieTrailerListener.onFailedGetMovieTrailer(S.ERROR_THROW);
                            }
                        } else {

                            if (response.body().getResultList() != null &&
                                    response.body().getResultList().size()>0) {
                                Log.e("hasil trailer", new Gson().toJson(response.body()));
                                getMovieTrailerListener.onSuccessGetMovieTrailer(response.body());

                            } else {

                                getMovieTrailerListener.onFailedGetMovieTrailer(S.ERROR_TRAILER_NOT_FOUND);
                            }
                        }
                    }
                    else {
                        getMovieTrailerListener.onFailedGetMovieTrailer(S.ERROR_THROW);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VideoModel> call, Throwable t) {
                    if(t.getMessage()!=null) {
                        getMovieTrailerListener.onFailedGetMovieTrailer(t.getMessage());
                    }
                    else {
                        getMovieTrailerListener.onFailedGetMovieTrailer(S.ERROR_THROW);
                    }
                }
            });
        }
        else {
            getMovieTrailerListener.onFailedGetMovieTrailer(S.ERROR_NO_INTERNET);
        }
    }

    public void setRestApi(RestApi restApi) {
        this.restApi = restApi;
    }

    public void setGetListMovieListener(NetworkingFunctionListener.GetListMovieListener getListMovieListener) {
        this.getListMovieListener = getListMovieListener;
    }

//    public void setGetListMovieTopRatedListener(NetworkingFunctionListener.GetListMovieTopRatedListener getListMovieTopRatedListener) {
//        this.getListMovieTopRatedListener = getListMovieTopRatedListener;
//    }
//
//    public void setGetListMovieSelectedListener(NetworkingFunctionListener.GetListMovieSelectedListener getListMovieSelectedListener) {
//        this.getListMovieSelectedListener = getListMovieSelectedListener;
//    }

    public void setGetMovieDetailListener(NetworkingFunctionListener.GetMovieDetailListener getMovieDetailListener) {
        this.getMovieDetailListener = getMovieDetailListener;
    }

    public void setGetMovieTrailerListener(NetworkingFunctionListener.GetMovieTrailerListener getMovieTrailerListener) {
        this.getMovieTrailerListener = getMovieTrailerListener;
    }

    public void setGetMovieCastListener(NetworkingFunctionListener.GetMovieCastListener getMovieCastListener) {
        this.getMovieCastListener = getMovieCastListener;
    }
}
