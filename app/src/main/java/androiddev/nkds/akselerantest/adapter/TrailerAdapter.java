package androiddev.nkds.akselerantest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.model.VideoModel;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.TrailerView> {

    private OnItemClickListener listener;
    private List<VideoModel.Result> resultList;
    private Context mContext;

    public TrailerAdapter(Context context){
        resultList = new ArrayList<>();
        mContext = context;

    }
    @NonNull
    @Override
    public TrailerAdapter.TrailerView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trailer,parent,false);
        return new TrailerView(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TrailerAdapter.TrailerView holder, int position) {

        holder.tvTitle.setText(resultList.get(position).getName());
        setImageToImageView(mContext,holder.ivTrailer, String.format(S.YOUTUBE_IMAGE_URL, resultList.get(position).getKey()));
    }

    class TrailerView extends RecyclerView.ViewHolder{

        private TextView tvTitle;
        private ImageView ivTrailer;

        public TrailerView(@NonNull View itemView) {
            super(itemView);

            ivTrailer = itemView.findViewById(R.id.iv_trailer);
            tvTitle = itemView.findViewById(R.id.tv_trailer_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null) {
                        listener.onClickListener(getAdapterPosition());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        if(resultList!=null && resultList.size()>0){
            return resultList.size();
        }
        return 0;
    }

    public List<VideoModel.Result> getListTrailer() {
        return resultList;
    }

    public void setResultList(List<VideoModel.Result> resultList) {
        this.resultList = resultList;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener{
        void onClickListener(int pos);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
}
