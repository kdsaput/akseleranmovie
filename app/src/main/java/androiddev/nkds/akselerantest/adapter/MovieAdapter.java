package androiddev.nkds.akselerantest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ListMovieModel.Result> listMovieModel;
    private OnItemClickListener listener;

    public MovieAdapter(Context context){
        listMovieModel = new ArrayList<>();
        mContext = context;

    }

    private boolean showProgressIndicator = true;
    public void setShowProgressIndicator(boolean showProgressIndicator) {
        this.showProgressIndicator = showProgressIndicator;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < listMovieModel.size()) {
            return ShowItemView.LAYOUT_RESOURCE;
        }
        return ProgressIndicatorView.LAYOUT_RESOURCE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType,parent,false);

        switch (viewType) {
            case ShowItemView.LAYOUT_RESOURCE:
                return new ShowItemView(itemView);
            case ProgressIndicatorView.LAYOUT_RESOURCE:
                return new ProgressIndicatorView(itemView);
        }
        throw new IllegalStateException("Invalid view type " + viewType + ".");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ShowItemView) {
            ShowItemView showItemView = (ShowItemView) holder;
            final ListMovieModel.Result provider = listMovieModel.get(position);
            setImageToImageView(mContext,showItemView.ivPoster, S.PATH_BASE_URL+provider.getPosterPath());
        }
    }

    class ShowItemView extends RecyclerView.ViewHolder {

        private static final int LAYOUT_RESOURCE = R.layout.item_movie;

        private ImageView ivPoster;

        ShowItemView(View v) {
            super(v);
            ivPoster = v.findViewById(R.id.iv_movie);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null) {
                        listener.onClickListener(getAdapterPosition());
                    }
                }
            });
        }
    }


    class ProgressIndicatorView extends RecyclerView.ViewHolder {

        private static final int LAYOUT_RESOURCE = R.layout.item_load_more;

        ProgressIndicatorView(@NonNull View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemCount() {

        if(listMovieModel!=null && listMovieModel.size()>0){
            if (showProgressIndicator) {
                return listMovieModel.size() + 1;
            }
            return listMovieModel.size();
        }
        return 0;
    }

    public List<ListMovieModel.Result> getListMovieModel() {
        return listMovieModel;
    }

    public void setListMovieModel(List<ListMovieModel.Result> listMovieModel) {
        this.listMovieModel = listMovieModel;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener{
        void onClickListener(int pos);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
}

