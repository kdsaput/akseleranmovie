package androiddev.nkds.akselerantest.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.model.ListMovieModel;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;

public class ImageSliderAdapter extends PagerAdapter {

    private Context context;
    private List<ListMovieModel.Result> resultList;
    private OnItemClickListener onItemClickListener;

    public ImageSliderAdapter(Context context, List<ListMovieModel.Result> resultList){
        this.context = context;
        this.resultList = resultList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_image_slider,container,false);

        if(view!=null){
            ImageView imageView = view.findViewById(R.id.iv_home_top);
            TextView textView = view.findViewById(R.id.tv_title);
            RelativeLayout relativeLayout = view.findViewById(R.id.ly_black);
            setImageToImageView(context,imageView, S.BACKDROP_BASE_URL + resultList.get(position).getBackdrop_path());
            textView.setText(resultList.get(position).getTitle());
            container.addView(view,0);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onClickListener(position);
                        }
                }
            });

            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onClickListener(position);
                    }
                }
            });

            return view;
        }

        return super.instantiateItem(container, position);
    }

    @Override
    public int getCount() {
        if(resultList!=null && resultList.size()>0){
            return resultList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public interface OnItemClickListener{

        void onClickListener(int pos);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
