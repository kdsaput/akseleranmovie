package androiddev.nkds.akselerantest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androiddev.nkds.akselerantest.R;
import androiddev.nkds.akselerantest.model.CastModel;
import androiddev.nkds.akselerantest.model.VideoModel;
import androiddev.nkds.akselerantest.utils.constants.S;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static androiddev.nkds.akselerantest.utils.helper.ImageHelper.setImageToImageView;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.TrailerView> {

    private OnItemClickListener listener;
    private List<CastModel.Cast> resultList;
    private Context mContext;

    public CastAdapter(Context context){
        resultList = new ArrayList<>();
        mContext = context;

    }
    @NonNull
    @Override
    public CastAdapter.TrailerView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cast,parent,false);
        return new TrailerView(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CastAdapter.TrailerView holder, int position) {

        setImageToImageView(mContext,holder.ivProfile, S.PATH_BASE_URL + resultList.get(position).getProfile_path());
    }

    class TrailerView extends RecyclerView.ViewHolder{

        private ImageView ivProfile;

        public TrailerView(@NonNull View itemView) {
            super(itemView);

            ivProfile = itemView.findViewById(R.id.iv_cast);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null) {
                        listener.onClickListener(getAdapterPosition());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        if(resultList!=null && resultList.size()>0){
            return resultList.size();
        }
        return 0;
    }

    public List<CastModel.Cast> getListTrailer() {
        return resultList;
    }

    public void setResultList(List<CastModel.Cast> resultList) {
        this.resultList = resultList;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener{
        void onClickListener(int pos);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
}
